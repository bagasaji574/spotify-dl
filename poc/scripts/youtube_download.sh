cd /tmp
orig_song_name=$1
echo $orig_song_name
song_name=$(echo "$orig_song_name" | tr " " +)
echo "looking for https://www.youtube.com/results/?search_query=$song_name"
curl -H "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36" -o search_page "https://www.youtube.com/results/?search_query=$song_name"
uri=$(grep -o "href=\"/watch?v=[^ ]*" search_page  | head -1 | grep -o "/.*[^\"]")
echo $uri
#youtube-dl -v -o "~/Music/Downloads/%(title)s.%(ext)s" -f bestaudio --extract-audio --audio-quality 0 "https://www.youtube.com$uri" 
download_link="https://savedeo.site/download?url=youtube.com$uri"
echo "Downloading from $download_link"


curl -H "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36" -o download_page $download_link

download=$(grep -o "href=\"https://r[^ ]*" download_page | tail -1 | grep -o  "https.*[^\"]")

echo "Song url is $download"

curl  -H "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36" $download -o "/home/tomer/Music/Downloads/$orig_song_name.mp3"

#rm search_page
#rm download_page

sleep $(( ( RANDOM % 15 )  + 6 ))
